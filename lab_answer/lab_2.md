1. XML merupakan markup language, sedangkan JSON merupakan data format.
JSON ditulis dalam bahasa JavaScript, XML adalah bahasa markup yang menggunakan tag untuk mendefinisikan elemen.
JSON menyimpan data dalam bentuk map, sementara XML menyimpannya dalam bentuk tree structure
JSON hanya mendukung tipe data primitif.

2. Berbeda dengan XML, HTML tidak case sensitive.
XML merupakan sebuah framework yang mendefinisikan markup language
HTML lebih bersifat static karena digunakan untuk mendisplay data
XML bersifat dynamic karena digunakan untuk mentransportasi data.
XML bisa mendefinisikan tag sesuai dengan kebutuhan programmer
