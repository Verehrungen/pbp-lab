from django.db import models

# Create your models here.

class Note(models.Model):
    To = models.CharField(max_length=16)
    From = models.CharField(max_length=16)
    Title = models.CharField(max_length=50)
    Message = models.CharField(max_length=280)